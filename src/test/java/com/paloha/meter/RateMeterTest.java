package com.paloha.meter;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

class RateMeterTest {

    @Test
    void incrementAndGetRateOnce() {
        RateMeter rateMeter = Mockito.spy(new RateMeter(1));
        long rate = rateMeter.incrementAndGetRate();
        Assertions.assertEquals(1L, rate);
    }

    @Test
    void incrementAndGetRateNTimes() {
        RateMeter rateMeter = Mockito.spy(new RateMeter(1));
        Mockito.when(rateMeter.currentSecond()).thenReturn(1234567890L);
        long rate = 0;
        for (int i = 0; i < 10000; i++) {
            rate = rateMeter.incrementAndGetRate();
        }
        Assertions.assertEquals(10000L, rate);
    }

    @Test
    void incrementOneTimeAndGetTotalRate() {
        RateMeter rateMeter = Mockito.spy(new RateMeter(1));
        Mockito.when(rateMeter.currentSecond()).thenReturn(1234567890L);
        rateMeter.incrementAndGetRate();
        Assertions.assertEquals(1L, rateMeter.getTotalRateFor(0));
    }

    @Test
    void incrementNTimesAndGetTotalRate() {
        RateMeter rateMeter = Mockito.spy(new RateMeter(1));
        Mockito.when(rateMeter.currentSecond()).thenReturn(1234567890L);
        for (int i = 0; i < 10000; i++) {
            rateMeter.incrementAndGetRate();
        }
        Assertions.assertEquals(10000L, rateMeter.getTotalRateFor(0));
    }

    @Test
    void incrementOneTimeAndGetMeanRate() {
        RateMeter rateMeter = Mockito.spy(new RateMeter(1));
        Mockito.when(rateMeter.currentSecond()).thenReturn(1234567890L);
        rateMeter.incrementAndGetRate();
        Assertions.assertEquals(1.0, rateMeter.getMeanRate());
    }

    @Test
    void incrementNTimeAndGetMeanRate() {
        RateMeter rateMeter = Mockito.spy(new RateMeter(1));
        Mockito.when(rateMeter.currentSecond()).thenReturn(1234567890L);
        for (int i = 0; i < 10000; i++) {
            rateMeter.incrementAndGetRate();
        }
        Assertions.assertEquals(10000.0, rateMeter.getMeanRate());
    }

    public static void main(String[] args) throws Exception {
        RateMeter rateMeter = new RateMeter(60);
        //cleaning thread
        new Thread(() -> {
            while (true) {
                //System.err.println("current size of rates: " + rates.size());
                rateMeter.trimToMemoryCapacity();
                try {
                    Thread.sleep(1000L);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }).start();
        //producer thread
        for (int i = 0; i < 16; i++) {
            new Thread(() -> {
                while (true) {
                    try {
                        Thread.sleep((long) (Math.random() * 10000.0 % 10000L));
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    rateMeter.incrementAndGetRate();
                }
            }).start();
        }
        //display thread
        new Thread(() -> {
            while (true) {
                try {
                    Thread.sleep(1000L);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                long totalRateFor10s = rateMeter.getTotalRateFor(10L);
                long totalRateFor20s = rateMeter.getTotalRateFor(20L);
                long totalRateFor30s = rateMeter.getTotalRateFor(30L);
                double meanRate = rateMeter.getMeanRate();
                System.out.println("10 seconds rate = " + totalRateFor10s + " --- 20 seconds rate = " + totalRateFor20s + " --- 30 seconds rate = " + totalRateFor30s + " --- mean rate = " + meanRate);
            }
        }).start();
    }
}