package com.paloha.meter;

import java.util.Collections;
import java.util.SortedMap;
import java.util.TreeMap;

public class RateMeter {

    private final SortedMap<Long, Long> rateMemory = Collections.synchronizedSortedMap(new TreeMap<>(Collections.reverseOrder()));
    private final int memoryCapacity;

    public RateMeter(int memoryCapacity) {
        if (memoryCapacity < 1) {
            throw new IllegalArgumentException("Memory capacity must be greater than 0.");
        }
        this.memoryCapacity = memoryCapacity;
    }

    public long currentSecond() {
        return System.currentTimeMillis() / 1000L;
    }

    public long incrementAndGetRate() {
        long currentSecond = currentSecond();
        long count = rateMemory.getOrDefault(currentSecond, 0L);
        rateMemory.put(currentSecond, ++count);
        return count;
    }

    public long getTotalRateFor(long pastSeconds) {
        long result = 0;
        long currentSecond = currentSecond();
        for (int i = 0; i <= pastSeconds; i++) {
            result = result + rateMemory.getOrDefault(currentSecond - i, 0L);
        }
        return result;
    }

    public double getMeanRate() {
        double result = 0.0;
        if (rateMemory.isEmpty()) {
            return result;
        }
        long currentSecond = currentSecond();
        long n = currentSecond - rateMemory.lastKey();
        double total = getTotalRateFor(n);
        return total / (n + 1L);
    }

    public void trimToMemoryCapacity() {
        while (rateMemory.size() > memoryCapacity) {
            rateMemory.remove(rateMemory.lastKey());
        }
    }
}
